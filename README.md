# Makewav 5.0

Makewav converts ROM image files for the Atari 2600 VCS into
.wav-files or plays them directly.

For compiling [PortAudio](http://www.portaudio.com/) is required.

DOS support has been removed.

## typical usage
```
makewav -l                                       # to get name of sound card
makewav -a <name_of_soundcard> -tc <romfile.bin> # for Cuttle Cart
makewav -a <name_of_soundcard> -ts <romfile.bin> # for SuperCharger
```

