
PROGRAM = makewav

CC       := gcc
CFLAGS   := -Os -g -Wall
#CFLAGS   := -g -Wall
LDFLAGS  := -lm -lportaudio
BUILDDIR := obj

all: $(BUILDDIR)/$(PROGRAM)

.FORCE:

$(BUILDDIR):
	mkdir -p $@

$(BUILDDIR)/%.o: src/%.c $(BUILDDIR)
	$(CC) $(CFLAGS) -c -o $@ $<

$(BUILDDIR)/$(PROGRAM): $(BUILDDIR)/queue.o $(BUILDDIR)/makewav.o $(BUILDDIR)/paplay.o $(BUILDDIR)/streambuffer.o
	$(CC) $(CFLAGS) -o $@ $^ $(LDFLAGS)

clean:
	rm -rf $(BUILDDIR)

debian/changelog: .FORCE
	dch -v $(shell echo -e '#include "version.h"\nMAKEWAV_VERSION' | gcc -E - |  grep -v '^#' | tr -d \")
